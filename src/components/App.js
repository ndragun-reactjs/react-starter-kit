import React from 'react'
import {Route, Switch, Redirect} from "react-router-dom";
import Auth from "../modules/auth/Auth";
import Error404 from "../modules/errors/404/404";
import Header from "./layout/Header";
import Home from "../modules/dashboard/home/Home";
import About from "../modules/dashboard/about/About";

const App = () => (
    <>
        {header(window.location.pathname)}
        <Switch>
            <Route exact path='/auth' component={Auth}/>
            <Redirect exact from='/' to='/dashboard'/>
            <Route path="/dashboard" component={Home} />
            <Route path="/about" component={About} />
            <Route component={Error404}/>
        </Switch>
    </>
);

function header(path) {
    const currentRoute = path.replace('/', '');
    // List of pages which won't load Header/Navbar
    const excludeList = ['auth'];
    const hideNavbar =  (excludeList.indexOf(currentRoute) > -1);
    if(!hideNavbar) return <Header />;
    else return null
}

export default App
