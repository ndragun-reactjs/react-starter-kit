import React from 'react'
import { NavLink } from 'react-router-dom'
import './Header.scss'

const Header = () => (
    <header>
        <nav>
            <ul>
                <li><NavLink to='/dashboard' exact activeClassName="active"><i className={'icon-monitor'}></i>Home</NavLink></li>
                <li><NavLink to='/about' activeClassName="active">About</NavLink></li>
                <li><NavLink to='/auth' activeClassName="active">Auth</NavLink></li>
            </ul>
        </nav>
    </header>
);

export default Header
